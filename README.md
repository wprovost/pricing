# Pricing service

This repo contains source code and a resource file that can be built into a Spring Boot REST service that can function as an alternative to the Yahoo! finance service. This can be a useful replacement if ...

* You want predictable pricing trends, for demonstration and/or testing purposes
* You want to be able to invoke a pricing service from your JavaScript front-end

## Building and running

You will need to create a Gradle project and add this source code and .csv file to it. You'll need just two Gradle dependencies:

* org.springframework.boot:spring-boot-starter-web:2.1.4.RELEASE
* org.springframework.boot:spring-boot-starter-test:2.1.4.RELEASE (test only)

You may need to adjust the server.port in *application.properties*.

Then just run com.citi.pricing.PriceServiceApplication.

See *BasePrices.csv* and comments in *com.citi.pricing.TrackedStock* to understand the more deterministic or random pricing results that you can expect for a given stock symbol.

